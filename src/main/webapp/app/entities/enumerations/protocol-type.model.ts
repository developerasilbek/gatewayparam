export enum ProtocolType {
  REST = 'REST',

  WS = 'WS',

  HESSIAN = 'HESSIAN',
}
