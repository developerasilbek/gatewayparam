export enum RequestType {
  CHECK = 'CHECK',

  PAY = 'PAY',

  STATUS = 'STATUS',

  CANCEL = 'CANCEL',

  DEPOSIT = 'DEPOSIT',

  RECONCILE = 'RECONCILE',
}
