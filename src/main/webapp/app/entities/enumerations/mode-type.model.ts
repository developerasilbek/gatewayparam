export enum ModeType {
  TEST = 'TEST',

  DEV = 'DEV',

  PROD = 'PROD',
}
