import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'gateway-param',
        data: { pageTitle: 'gatewayParamApp.bmmsGatewayParam.home.title' },
        loadChildren: () => import('./bmms/gateway-param/gateway-param.module').then(m => m.PkbillmsGatewayParamModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
