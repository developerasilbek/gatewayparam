import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IGatewayParam } from '../gateway-param.model';
import { GatewayParamService } from '../service/gateway-param.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './gateway-param-delete-dialog.component.html',
})
export class GatewayParamDeleteDialogComponent {
  gatewayParam?: IGatewayParam;

  constructor(protected gatewayParamService: GatewayParamService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.gatewayParamService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
