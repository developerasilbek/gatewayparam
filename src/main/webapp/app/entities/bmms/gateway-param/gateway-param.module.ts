import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { GatewayParamComponent } from './list/gateway-param.component';
import { GatewayParamDetailComponent } from './detail/gateway-param-detail.component';
import { GatewayParamUpdateComponent } from './update/gateway-param-update.component';
import { GatewayParamDeleteDialogComponent } from './delete/gateway-param-delete-dialog.component';
import { GatewayParamRoutingModule } from './route/gateway-param-routing.module';

@NgModule({
  imports: [SharedModule, GatewayParamRoutingModule],
  declarations: [GatewayParamComponent, GatewayParamDetailComponent, GatewayParamUpdateComponent, GatewayParamDeleteDialogComponent],
})
export class PkbillmsGatewayParamModule {}
