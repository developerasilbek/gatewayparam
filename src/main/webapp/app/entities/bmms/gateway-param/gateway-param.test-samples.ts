import { RequestType } from 'app/entities/enumerations/request-type.model';
import { MethodType } from 'app/entities/enumerations/method-type.model';
import { ProtocolType } from 'app/entities/enumerations/protocol-type.model';
import { ModeType } from 'app/entities/enumerations/mode-type.model';

import { IGatewayParam, NewGatewayParam } from './gateway-param.model';

export const sampleWithRequiredData: IGatewayParam = {
  id: 75704,
  active: false,
};

export const sampleWithPartialData: IGatewayParam = {
  id: 96059,
  url: 'http://adriana.name',
  username: 'gold Borders Advanced',
  body: 'hacking',
  statusQuery: 'Ameliorated',
  callback: 'payment implement Australia',
  timeout: 9832,
  dataQueries: 'Principal Senior morph',
  protocol: ProtocolType['REST'],
  requestMediaType: 'Bike',
  active: true,
  mode: ModeType['DEV'],
  extIdQuery: 'Object-based open-source California',
};

export const sampleWithFullData: IGatewayParam = {
  id: 56163,
  url: 'http://thurman.name',
  type: RequestType['RECONCILE'],
  username: 'Fresh',
  password: 'Tuna Mouse green',
  token: 'Loan',
  body: 'markets JSON',
  statusQuery: 'Account',
  messageQuery: 'asymmetric black generating',
  successCode: 'Small',
  callback: 'Tuna Up-sized Rustic',
  timeout: 65454,
  headers: 'Colorado Michigan',
  dataQueries: 'Iraq monitor',
  httpMethodType: MethodType['PUT'],
  protocol: ProtocolType['REST'],
  requestMediaType: 'CSS',
  responseMediaType: 'Row HTTP Pants',
  active: false,
  mode: ModeType['TEST'],
  extIdQuery: 'Fresh',
  successOn2xx: true,
};

export const sampleWithNewData: NewGatewayParam = {
  active: false,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
