import { RequestType } from 'app/entities/enumerations/request-type.model';
import { MethodType } from 'app/entities/enumerations/method-type.model';
import { ProtocolType } from 'app/entities/enumerations/protocol-type.model';
import { ModeType } from 'app/entities/enumerations/mode-type.model';

export interface IGatewayParam {
  id: number;
  url?: string | null;
  type?: RequestType | null;
  username?: string | null;
  password?: string | null;
  token?: string | null;
  body?: string | null;
  statusQuery?: string | null;
  messageQuery?: string | null;
  successCode?: string | null;
  callback?: string | null;
  timeout?: number | null;
  headers?: string | null;
  dataQueries?: string | null;
  httpMethodType?: MethodType | null;
  protocol?: ProtocolType | null;
  requestMediaType?: string | null;
  responseMediaType?: string | null;
  active?: boolean | null;
  mode?: ModeType | null;
  extIdQuery?: string | null;
  successOn2xx?: boolean | null;
}

export type NewGatewayParam = Omit<IGatewayParam, 'id'> & { id: null };
