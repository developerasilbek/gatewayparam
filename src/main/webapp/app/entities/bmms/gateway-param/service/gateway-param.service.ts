import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IGatewayParam, NewGatewayParam } from '../gateway-param.model';

export type PartialUpdateGatewayParam = Partial<IGatewayParam> & Pick<IGatewayParam, 'id'>;

export type EntityResponseType = HttpResponse<IGatewayParam>;
export type EntityArrayResponseType = HttpResponse<IGatewayParam[]>;

@Injectable({ providedIn: 'root' })
export class GatewayParamService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/gatewayparams');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(gatewayParam: NewGatewayParam): Observable<EntityResponseType> {
    return this.http.post<IGatewayParam>(this.resourceUrl, gatewayParam, { observe: 'response' });
  }

  update(gatewayParam: IGatewayParam): Observable<EntityResponseType> {
    return this.http.put<IGatewayParam>(`${this.resourceUrl}/${this.getGatewayParamIdentifier(gatewayParam)}`, gatewayParam, {
      observe: 'response',
    });
  }

  partialUpdate(gatewayParam: PartialUpdateGatewayParam): Observable<EntityResponseType> {
    return this.http.patch<IGatewayParam>(`${this.resourceUrl}/${this.getGatewayParamIdentifier(gatewayParam)}`, gatewayParam, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IGatewayParam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGatewayParam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getGatewayParamIdentifier(gatewayParam: Pick<IGatewayParam, 'id'>): number {
    return gatewayParam.id;
  }

  compareGatewayParam(o1: Pick<IGatewayParam, 'id'> | null, o2: Pick<IGatewayParam, 'id'> | null): boolean {
    return o1 && o2 ? this.getGatewayParamIdentifier(o1) === this.getGatewayParamIdentifier(o2) : o1 === o2;
  }

  addGatewayParamToCollectionIfMissing<Type extends Pick<IGatewayParam, 'id'>>(
    gatewayParamCollection: Type[],
    ...gatewayParamsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const gatewayParams: Type[] = gatewayParamsToCheck.filter(isPresent);
    if (gatewayParams.length > 0) {
      const gatewayParamCollectionIdentifiers = gatewayParamCollection.map(
        gatewayParamItem => this.getGatewayParamIdentifier(gatewayParamItem)!
      );
      const gatewayParamsToAdd = gatewayParams.filter(gatewayParamItem => {
        const gatewayParamIdentifier = this.getGatewayParamIdentifier(gatewayParamItem);
        if (gatewayParamCollectionIdentifiers.includes(gatewayParamIdentifier)) {
          return false;
        }
        gatewayParamCollectionIdentifiers.push(gatewayParamIdentifier);
        return true;
      });
      return [...gatewayParamsToAdd, ...gatewayParamCollection];
    }
    return gatewayParamCollection;
  }
}
