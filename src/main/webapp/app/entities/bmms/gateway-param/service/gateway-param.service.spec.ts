import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IGatewayParam } from '../gateway-param.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../gateway-param.test-samples';

import { GatewayParamService } from './gateway-param.service';

const requireRestSample: IGatewayParam = {
  ...sampleWithRequiredData,
};

describe('GatewayParam Service', () => {
  let service: GatewayParamService;
  let httpMock: HttpTestingController;
  let expectedResult: IGatewayParam | IGatewayParam[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(GatewayParamService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a GatewayParam', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const gatewayParam = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(gatewayParam).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a GatewayParam', () => {
      const gatewayParam = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(gatewayParam).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a GatewayParam', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of GatewayParam', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a GatewayParam', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addGatewayParamToCollectionIfMissing', () => {
      it('should add a GatewayParam to an empty array', () => {
        const gatewayParam: IGatewayParam = sampleWithRequiredData;
        expectedResult = service.addGatewayParamToCollectionIfMissing([], gatewayParam);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(gatewayParam);
      });

      it('should not add a GatewayParam to an array that contains it', () => {
        const gatewayParam: IGatewayParam = sampleWithRequiredData;
        const gatewayParamCollection: IGatewayParam[] = [
          {
            ...gatewayParam,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addGatewayParamToCollectionIfMissing(gatewayParamCollection, gatewayParam);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a GatewayParam to an array that doesn't contain it", () => {
        const gatewayParam: IGatewayParam = sampleWithRequiredData;
        const gatewayParamCollection: IGatewayParam[] = [sampleWithPartialData];
        expectedResult = service.addGatewayParamToCollectionIfMissing(gatewayParamCollection, gatewayParam);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(gatewayParam);
      });

      it('should add only unique GatewayParam to an array', () => {
        const gatewayParamArray: IGatewayParam[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const gatewayParamCollection: IGatewayParam[] = [sampleWithRequiredData];
        expectedResult = service.addGatewayParamToCollectionIfMissing(gatewayParamCollection, ...gatewayParamArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const gatewayParam: IGatewayParam = sampleWithRequiredData;
        const gatewayParam2: IGatewayParam = sampleWithPartialData;
        expectedResult = service.addGatewayParamToCollectionIfMissing([], gatewayParam, gatewayParam2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(gatewayParam);
        expect(expectedResult).toContain(gatewayParam2);
      });

      it('should accept null and undefined values', () => {
        const gatewayParam: IGatewayParam = sampleWithRequiredData;
        expectedResult = service.addGatewayParamToCollectionIfMissing([], null, gatewayParam, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(gatewayParam);
      });

      it('should return initial array if no GatewayParam is added', () => {
        const gatewayParamCollection: IGatewayParam[] = [sampleWithRequiredData];
        expectedResult = service.addGatewayParamToCollectionIfMissing(gatewayParamCollection, undefined, null);
        expect(expectedResult).toEqual(gatewayParamCollection);
      });
    });

    describe('compareGatewayParam', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareGatewayParam(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareGatewayParam(entity1, entity2);
        const compareResult2 = service.compareGatewayParam(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareGatewayParam(entity1, entity2);
        const compareResult2 = service.compareGatewayParam(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareGatewayParam(entity1, entity2);
        const compareResult2 = service.compareGatewayParam(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
