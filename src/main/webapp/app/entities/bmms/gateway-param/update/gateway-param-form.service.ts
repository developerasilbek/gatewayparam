import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IGatewayParam, NewGatewayParam } from '../gateway-param.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IGatewayParam for edit and NewGatewayParamFormGroupInput for create.
 */
type GatewayParamFormGroupInput = IGatewayParam | PartialWithRequiredKeyOf<NewGatewayParam>;

type GatewayParamFormDefaults = Pick<NewGatewayParam, 'id' | 'active' | 'successOn2xx'>;

type GatewayParamFormGroupContent = {
  id: FormControl<IGatewayParam['id'] | NewGatewayParam['id']>;
  url: FormControl<IGatewayParam['url']>;
  type: FormControl<IGatewayParam['type']>;
  username: FormControl<IGatewayParam['username']>;
  password: FormControl<IGatewayParam['password']>;
  token: FormControl<IGatewayParam['token']>;
  body: FormControl<IGatewayParam['body']>;
  statusQuery: FormControl<IGatewayParam['statusQuery']>;
  messageQuery: FormControl<IGatewayParam['messageQuery']>;
  successCode: FormControl<IGatewayParam['successCode']>;
  callback: FormControl<IGatewayParam['callback']>;
  timeout: FormControl<IGatewayParam['timeout']>;
  headers: FormControl<IGatewayParam['headers']>;
  dataQueries: FormControl<IGatewayParam['dataQueries']>;
  httpMethodType: FormControl<IGatewayParam['httpMethodType']>;
  protocol: FormControl<IGatewayParam['protocol']>;
  requestMediaType: FormControl<IGatewayParam['requestMediaType']>;
  responseMediaType: FormControl<IGatewayParam['responseMediaType']>;
  active: FormControl<IGatewayParam['active']>;
  mode: FormControl<IGatewayParam['mode']>;
  extIdQuery: FormControl<IGatewayParam['extIdQuery']>;
  successOn2xx: FormControl<IGatewayParam['successOn2xx']>;
};

export type GatewayParamFormGroup = FormGroup<GatewayParamFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class GatewayParamFormService {
  createGatewayParamFormGroup(gatewayParam: GatewayParamFormGroupInput = { id: null }): GatewayParamFormGroup {
    const gatewayParamRawValue = {
      ...this.getFormDefaults(),
      ...gatewayParam,
    };
    return new FormGroup<GatewayParamFormGroupContent>({
      id: new FormControl(
        { value: gatewayParamRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      url: new FormControl(gatewayParamRawValue.url, {
        validators: [Validators.maxLength(512)],
      }),
      type: new FormControl(gatewayParamRawValue.type),
      username: new FormControl(gatewayParamRawValue.username, {
        validators: [Validators.maxLength(32)],
      }),
      password: new FormControl(gatewayParamRawValue.password, {
        validators: [Validators.maxLength(64)],
      }),
      token: new FormControl(gatewayParamRawValue.token, {
        validators: [Validators.maxLength(128)],
      }),
      body: new FormControl(gatewayParamRawValue.body, {
        validators: [Validators.maxLength(2048)],
      }),
      statusQuery: new FormControl(gatewayParamRawValue.statusQuery, {
        validators: [Validators.maxLength(256)],
      }),
      messageQuery: new FormControl(gatewayParamRawValue.messageQuery, {
        validators: [Validators.maxLength(256)],
      }),
      successCode: new FormControl(gatewayParamRawValue.successCode, {
        validators: [Validators.maxLength(32)],
      }),
      callback: new FormControl(gatewayParamRawValue.callback, {
        validators: [Validators.maxLength(512)],
      }),
      timeout: new FormControl(gatewayParamRawValue.timeout),
      headers: new FormControl(gatewayParamRawValue.headers, {
        validators: [Validators.maxLength(1024)],
      }),
      dataQueries: new FormControl(gatewayParamRawValue.dataQueries, {
        validators: [Validators.maxLength(1024)],
      }),
      httpMethodType: new FormControl(gatewayParamRawValue.httpMethodType),
      protocol: new FormControl(gatewayParamRawValue.protocol),
      requestMediaType: new FormControl(gatewayParamRawValue.requestMediaType, {
        validators: [Validators.maxLength(64)],
      }),
      responseMediaType: new FormControl(gatewayParamRawValue.responseMediaType, {
        validators: [Validators.maxLength(64)],
      }),
      active: new FormControl(gatewayParamRawValue.active, {
        validators: [Validators.required],
      }),
      mode: new FormControl(gatewayParamRawValue.mode),
      extIdQuery: new FormControl(gatewayParamRawValue.extIdQuery),
      successOn2xx: new FormControl(gatewayParamRawValue.successOn2xx),
    });
  }

  getGatewayParam(form: GatewayParamFormGroup): IGatewayParam | NewGatewayParam {
    return form.getRawValue() as IGatewayParam | NewGatewayParam;
  }

  resetForm(form: GatewayParamFormGroup, gatewayParam: GatewayParamFormGroupInput): void {
    const gatewayParamRawValue = { ...this.getFormDefaults(), ...gatewayParam };
    form.reset(
      {
        ...gatewayParamRawValue,
        id: { value: gatewayParamRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): GatewayParamFormDefaults {
    return {
      id: null,
      active: false,
      successOn2xx: false,
    };
  }
}
