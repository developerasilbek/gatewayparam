import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { GatewayParamFormService, GatewayParamFormGroup } from './gateway-param-form.service';
import { IGatewayParam } from '../gateway-param.model';
import { GatewayParamService } from '../service/gateway-param.service';
import { RequestType } from 'app/entities/enumerations/request-type.model';
import { MethodType } from 'app/entities/enumerations/method-type.model';
import { ProtocolType } from 'app/entities/enumerations/protocol-type.model';
import { ModeType } from 'app/entities/enumerations/mode-type.model';

@Component({
  selector: 'jhi-gateway-param-update',
  templateUrl: './gateway-param-update.component.html',
})
export class GatewayParamUpdateComponent implements OnInit {
  isSaving = false;
  gatewayParam: IGatewayParam | null = null;
  requestTypeValues = Object.keys(RequestType);
  methodTypeValues = Object.keys(MethodType);
  protocolTypeValues = Object.keys(ProtocolType);
  modeTypeValues = Object.keys(ModeType);

  editForm: GatewayParamFormGroup = this.gatewayParamFormService.createGatewayParamFormGroup();

  constructor(
    protected gatewayParamService: GatewayParamService,
    protected gatewayParamFormService: GatewayParamFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ gatewayParam }) => {
      this.gatewayParam = gatewayParam;
      if (gatewayParam) {
        this.updateForm(gatewayParam);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const gatewayParam = this.gatewayParamFormService.getGatewayParam(this.editForm);
    if (gatewayParam.id !== null) {
      this.subscribeToSaveResponse(this.gatewayParamService.update(gatewayParam));
    } else {
      this.subscribeToSaveResponse(this.gatewayParamService.create(gatewayParam));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGatewayParam>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(gatewayParam: IGatewayParam): void {
    this.gatewayParam = gatewayParam;
    this.gatewayParamFormService.resetForm(this.editForm, gatewayParam);
  }
}
