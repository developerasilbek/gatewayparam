import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GatewayParamFormService } from './gateway-param-form.service';
import { GatewayParamService } from '../service/gateway-param.service';
import { IGatewayParam } from '../gateway-param.model';

import { GatewayParamUpdateComponent } from './gateway-param-update.component';

describe('GatewayParam Management Update Component', () => {
  let comp: GatewayParamUpdateComponent;
  let fixture: ComponentFixture<GatewayParamUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let gatewayParamFormService: GatewayParamFormService;
  let gatewayParamService: GatewayParamService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GatewayParamUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GatewayParamUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GatewayParamUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    gatewayParamFormService = TestBed.inject(GatewayParamFormService);
    gatewayParamService = TestBed.inject(GatewayParamService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const gatewayParam: IGatewayParam = { id: 456 };

      activatedRoute.data = of({ gatewayParam });
      comp.ngOnInit();

      expect(comp.gatewayParam).toEqual(gatewayParam);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGatewayParam>>();
      const gatewayParam = { id: 123 };
      jest.spyOn(gatewayParamFormService, 'getGatewayParam').mockReturnValue(gatewayParam);
      jest.spyOn(gatewayParamService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ gatewayParam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: gatewayParam }));
      saveSubject.complete();

      // THEN
      expect(gatewayParamFormService.getGatewayParam).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(gatewayParamService.update).toHaveBeenCalledWith(expect.objectContaining(gatewayParam));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGatewayParam>>();
      const gatewayParam = { id: 123 };
      jest.spyOn(gatewayParamFormService, 'getGatewayParam').mockReturnValue({ id: null });
      jest.spyOn(gatewayParamService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ gatewayParam: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: gatewayParam }));
      saveSubject.complete();

      // THEN
      expect(gatewayParamFormService.getGatewayParam).toHaveBeenCalled();
      expect(gatewayParamService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGatewayParam>>();
      const gatewayParam = { id: 123 };
      jest.spyOn(gatewayParamService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ gatewayParam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(gatewayParamService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
