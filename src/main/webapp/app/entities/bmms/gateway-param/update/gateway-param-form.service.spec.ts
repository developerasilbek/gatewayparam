import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../gateway-param.test-samples';

import { GatewayParamFormService } from './gateway-param-form.service';

describe('GatewayParam Form Service', () => {
  let service: GatewayParamFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GatewayParamFormService);
  });

  describe('Service methods', () => {
    describe('createGatewayParamFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createGatewayParamFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            url: expect.any(Object),
            type: expect.any(Object),
            username: expect.any(Object),
            password: expect.any(Object),
            token: expect.any(Object),
            body: expect.any(Object),
            statusQuery: expect.any(Object),
            messageQuery: expect.any(Object),
            successCode: expect.any(Object),
            callback: expect.any(Object),
            timeout: expect.any(Object),
            headers: expect.any(Object),
            dataQueries: expect.any(Object),
            httpMethodType: expect.any(Object),
            protocol: expect.any(Object),
            requestMediaType: expect.any(Object),
            responseMediaType: expect.any(Object),
            active: expect.any(Object),
            mode: expect.any(Object),
            extIdQuery: expect.any(Object),
            successOn2xx: expect.any(Object),
          })
        );
      });

      it('passing IGatewayParam should create a new form with FormGroup', () => {
        const formGroup = service.createGatewayParamFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            url: expect.any(Object),
            type: expect.any(Object),
            username: expect.any(Object),
            password: expect.any(Object),
            token: expect.any(Object),
            body: expect.any(Object),
            statusQuery: expect.any(Object),
            messageQuery: expect.any(Object),
            successCode: expect.any(Object),
            callback: expect.any(Object),
            timeout: expect.any(Object),
            headers: expect.any(Object),
            dataQueries: expect.any(Object),
            httpMethodType: expect.any(Object),
            protocol: expect.any(Object),
            requestMediaType: expect.any(Object),
            responseMediaType: expect.any(Object),
            active: expect.any(Object),
            mode: expect.any(Object),
            extIdQuery: expect.any(Object),
            successOn2xx: expect.any(Object),
          })
        );
      });
    });

    describe('getGatewayParam', () => {
      it('should return NewGatewayParam for default GatewayParam initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createGatewayParamFormGroup(sampleWithNewData);

        const gatewayParam = service.getGatewayParam(formGroup) as any;

        expect(gatewayParam).toMatchObject(sampleWithNewData);
      });

      it('should return NewGatewayParam for empty GatewayParam initial value', () => {
        const formGroup = service.createGatewayParamFormGroup();

        const gatewayParam = service.getGatewayParam(formGroup) as any;

        expect(gatewayParam).toMatchObject({});
      });

      it('should return IGatewayParam', () => {
        const formGroup = service.createGatewayParamFormGroup(sampleWithRequiredData);

        const gatewayParam = service.getGatewayParam(formGroup) as any;

        expect(gatewayParam).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IGatewayParam should not enable id FormControl', () => {
        const formGroup = service.createGatewayParamFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewGatewayParam should disable id FormControl', () => {
        const formGroup = service.createGatewayParamFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
