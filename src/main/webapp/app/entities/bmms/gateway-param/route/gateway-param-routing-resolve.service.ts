import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IGatewayParam } from '../gateway-param.model';
import { GatewayParamService } from '../service/gateway-param.service';

@Injectable({ providedIn: 'root' })
export class GatewayParamRoutingResolveService implements Resolve<IGatewayParam | null> {
  constructor(protected service: GatewayParamService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IGatewayParam | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((gatewayParam: HttpResponse<IGatewayParam>) => {
          if (gatewayParam.body) {
            return of(gatewayParam.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
