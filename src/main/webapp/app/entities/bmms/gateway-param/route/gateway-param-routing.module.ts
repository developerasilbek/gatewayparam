import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { GatewayParamComponent } from '../list/gateway-param.component';
import { GatewayParamDetailComponent } from '../detail/gateway-param-detail.component';
import { GatewayParamUpdateComponent } from '../update/gateway-param-update.component';
import { GatewayParamRoutingResolveService } from './gateway-param-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const gatewayParamRoute: Routes = [
  {
    path: '',
    component: GatewayParamComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: GatewayParamDetailComponent,
    resolve: {
      gatewayParam: GatewayParamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: GatewayParamUpdateComponent,
    resolve: {
      gatewayParam: GatewayParamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: GatewayParamUpdateComponent,
    resolve: {
      gatewayParam: GatewayParamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(gatewayParamRoute)],
  exports: [RouterModule],
})
export class GatewayParamRoutingModule {}
