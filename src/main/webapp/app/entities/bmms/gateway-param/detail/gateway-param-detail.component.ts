import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGatewayParam } from '../gateway-param.model';

@Component({
  selector: 'jhi-gateway-param-detail',
  templateUrl: './gateway-param-detail.component.html',
})
export class GatewayParamDetailComponent implements OnInit {
  gatewayParam: IGatewayParam | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ gatewayParam }) => {
      this.gatewayParam = gatewayParam;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
