import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayParamDetailComponent } from './gateway-param-detail.component';

describe('GatewayParam Management Detail Component', () => {
  let comp: GatewayParamDetailComponent;
  let fixture: ComponentFixture<GatewayParamDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GatewayParamDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ gatewayParam: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(GatewayParamDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(GatewayParamDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load gatewayParam on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.gatewayParam).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
