package uz.devops.domain.enumeration;

/**
 * The ProtocolType enumeration.
 */
public enum ProtocolType {
    REST,
    WS,
    HESSIAN,
}
