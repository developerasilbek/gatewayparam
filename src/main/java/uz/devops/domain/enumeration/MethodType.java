package uz.devops.domain.enumeration;

/**
 * The MethodType enumeration.
 */
public enum MethodType {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
}
