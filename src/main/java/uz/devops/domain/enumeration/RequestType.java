package uz.devops.domain.enumeration;

/**
 * The RequestType enumeration.
 */
public enum RequestType {
    CHECK,
    PAY,
    STATUS,
    CANCEL,
    DEPOSIT,
    RECONCILE,
}
