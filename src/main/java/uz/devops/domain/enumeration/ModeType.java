package uz.devops.domain.enumeration;

/**
 * The ModeType enumeration.
 */
public enum ModeType {
    TEST,
    DEV,
    PROD,
}
