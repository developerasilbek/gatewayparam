package uz.devops.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.devops.domain.GatewayParam;

/**
 * Spring Data JPA repository for the GatewayParam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GatewayParamRepository extends JpaRepository<GatewayParam, Long>, JpaSpecificationExecutor<GatewayParam> {}
