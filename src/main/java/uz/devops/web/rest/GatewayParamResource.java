package uz.devops.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.devops.repository.GatewayParamRepository;
import uz.devops.service.GatewayParamQueryService;
import uz.devops.service.criteria.GatewayParamCriteria;
import uz.devops.service.dto.GatewayParamDTO;
import uz.devops.service.impl.GatewayParamServiceImpl;
import uz.devops.web.rest.errors.BadRequestAlertException;

@RestController
@RequestMapping("/api")
public class GatewayParamResource {

    private final Logger log = LoggerFactory.getLogger(GatewayParamResource.class);

    private static final String ENTITY_NAME = "bmmsGatewayParam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GatewayParamServiceImpl gatewayParamService;

    private final GatewayParamRepository gatewayParamRepository;

    private final GatewayParamQueryService gatewayParamQueryService;

    public GatewayParamResource(
        GatewayParamServiceImpl gatewayParamService,
        GatewayParamRepository gatewayParamRepository,
        GatewayParamQueryService gatewayParamQueryService
    ) {
        this.gatewayParamService = gatewayParamService;
        this.gatewayParamRepository = gatewayParamRepository;
        this.gatewayParamQueryService = gatewayParamQueryService;
    }

    @PostMapping("/gatewayparams")
    public ResponseEntity<GatewayParamDTO> createGatewayParam(@Valid @RequestBody GatewayParamDTO gatewayParamDTO)
        throws URISyntaxException {
        log.debug("REST request to save GatewayParam : {}", gatewayParamDTO);
        GatewayParamDTO result = gatewayParamService.save(gatewayParamDTO);
        return ResponseEntity
            .created(new URI("/api/gatewayparams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/gatewayparams/savelist")
    public ResponseEntity<String> saveListGatewayParams(@Valid @RequestBody List<GatewayParamDTO> gatewayParamList) {
        if (gatewayParamList == null) {
            return ResponseEntity.badRequest().body("ERROR!");
        }

        log.debug("REST request to save GatewayParamList : {}", gatewayParamList);

        for (GatewayParamDTO gatewayParamDTO : gatewayParamList) {
            if (gatewayParamDTO != null) {
                gatewayParamService.save(gatewayParamDTO);
            }
        }
        return ResponseEntity.ok("SUCCESS!");
    }

    @PostMapping("/file/checks")
    public ResponseEntity<Boolean> checkFile(@RequestParam("file") MultipartFile file) throws URISyntaxException {
        log.debug("REST request to check PostmanCollection : {}", file);
        Boolean isPostmanCollection = gatewayParamService.checkFile(file);
        return ResponseEntity
            .created(new URI("/api/file/checks/" + isPostmanCollection))
            .headers(
                HeaderUtil.createEntityCreationAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    isPostmanCollection ? "file is postman collection" : "not postman collection"
                )
            )
            .body(isPostmanCollection);
    }

    @PostMapping("/gatewayparams/postman")
    public ResponseEntity<List<GatewayParamDTO>> createGatewayParamPostman(@RequestParam("file") MultipartFile file)
        throws URISyntaxException, IOException {
        log.debug("REST request to save File : {}", file);
        List<GatewayParamDTO> result = gatewayParamService.save(file);
        return ResponseEntity
            .created(new URI("/api/gatewayparams/postman/" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, String.valueOf(result.size())))
            .body(result);
    }

    @PutMapping("/gatewayparams/{id}")
    public ResponseEntity<GatewayParamDTO> updateGatewayParam(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody GatewayParamDTO gatewayParamDTO
    ) throws URISyntaxException {
        log.debug("REST request to update GatewayParam : {}, {}", id, gatewayParamDTO);
        if (gatewayParamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, gatewayParamDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!gatewayParamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GatewayParamDTO result = gatewayParamService.update(gatewayParamDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, gatewayParamDTO.getId().toString()))
            .body(result);
    }

    @PatchMapping(value = "/gatewayparams/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GatewayParamDTO> partialUpdateGatewayParam(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody GatewayParamDTO gatewayParamDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update GatewayParam partially : {}, {}", id, gatewayParamDTO);
        if (gatewayParamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, gatewayParamDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!gatewayParamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GatewayParamDTO> result = gatewayParamService.partialUpdate(gatewayParamDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, gatewayParamDTO.getId().toString())
        );
    }

    @GetMapping("/gatewayparams")
    public ResponseEntity<List<GatewayParamDTO>> getAllGatewayParams(
        GatewayParamCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get GatewayParams by criteria: {}", criteria);
        Page<GatewayParamDTO> page = gatewayParamQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/gatewayparams/count")
    public ResponseEntity<Long> countGatewayParams(GatewayParamCriteria criteria) {
        log.debug("REST request to count GatewayParams by criteria: {}", criteria);
        return ResponseEntity.ok().body(gatewayParamQueryService.countByCriteria(criteria));
    }

    @GetMapping("/gatewayparams/{id}")
    public ResponseEntity<GatewayParamDTO> getGatewayParam(@PathVariable Long id) {
        log.debug("REST request to get GatewayParam : {}", id);
        Optional<GatewayParamDTO> gatewayParamDTO = gatewayParamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(gatewayParamDTO);
    }

    @DeleteMapping("/gatewayparams/{id}")
    public ResponseEntity<Void> deleteGatewayParam(@PathVariable Long id) {
        log.debug("REST request to delete GatewayParam : {}", id);
        gatewayParamService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
