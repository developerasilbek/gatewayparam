package uz.devops.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.devops.domain.GatewayParam;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.service.impl.PmParserServiceImpl;
import uz.devops.repository.GatewayParamRepository;
import uz.devops.service.GatewayParamService;
import uz.devops.service.dto.GatewayParamDTO;
import uz.devops.service.mapper.GatewayParamMapper;
import uz.devops.service.mapper.PmParserToGatewayParam;

@Service
@Transactional
public class GatewayParamServiceImpl implements GatewayParamService {

    private final Logger log = LoggerFactory.getLogger(GatewayParamService.class);

    private final GatewayParamRepository gatewayParamRepository;

    private final GatewayParamMapper gatewayParamMapper;

    private final PmParserToGatewayParam pmParserToGatewayParam;

    private final PmParserServiceImpl pmParserService;

    public GatewayParamServiceImpl(
        GatewayParamRepository gatewayParamRepository,
        GatewayParamMapper gatewayParamMapper,
        PmParserToGatewayParam pmParserToGatewayParam,
        PmParserServiceImpl pmParserService
    ) {
        this.gatewayParamRepository = gatewayParamRepository;
        this.gatewayParamMapper = gatewayParamMapper;
        this.pmParserToGatewayParam = pmParserToGatewayParam;
        this.pmParserService = pmParserService;
    }

    @Override
    public GatewayParamDTO save(GatewayParamDTO gatewayParamDTO) {
        log.debug("Request to save GatewayParam : {}", gatewayParamDTO);
        GatewayParam gatewayParam = gatewayParamMapper.toEntity(gatewayParamDTO);
        gatewayParam = gatewayParamRepository.save(gatewayParam);
        return gatewayParamMapper.toDto(gatewayParam);
    }

    @Override
    public List<GatewayParamDTO> save(MultipartFile file) throws IOException {
        log.debug("Request for parsing Postman collection : {}", file);
        List<PmParser> pmParserList = pmParserService.parse(file);
        List<GatewayParam> gatewayParamList = pmParserToGatewayParam.toEntity(pmParserList);
        //        gatewayParamList = gatewayParamRepository.saveAll(gatewayParamList);
        return gatewayParamMapper.toDto(gatewayParamList);
    }

    @Override
    public Boolean checkFile(MultipartFile file) {
        log.debug("Request to check file : {}", file);
        return pmParserService.checkFile(file);
    }

    @Override
    public GatewayParamDTO update(GatewayParamDTO gatewayParamDTO) {
        log.debug("Request to update GatewayParam : {}", gatewayParamDTO);
        GatewayParam gatewayParam = gatewayParamMapper.toEntity(gatewayParamDTO);
        gatewayParam = gatewayParamRepository.save(gatewayParam);
        return gatewayParamMapper.toDto(gatewayParam);
    }

    @Override
    public Optional<GatewayParamDTO> partialUpdate(GatewayParamDTO gatewayParamDTO) {
        log.debug("Request to partially update GatewayParam : {}", gatewayParamDTO);

        return gatewayParamRepository
            .findById(gatewayParamDTO.getId())
            .map(existingGatewayParam -> {
                gatewayParamMapper.partialUpdate(existingGatewayParam, gatewayParamDTO);

                return existingGatewayParam;
            })
            .map(gatewayParamRepository::save)
            .map(gatewayParamMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GatewayParamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GatewayParams");
        return gatewayParamRepository.findAll(pageable).map(gatewayParamMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<GatewayParamDTO> findOne(Long id) {
        log.debug("Request to get GatewayParam : {}", id);
        return gatewayParamRepository.findById(id).map(gatewayParamMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete GatewayParam : {}", id);
        gatewayParamRepository.deleteById(id);
    }
}
