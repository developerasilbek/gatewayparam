package uz.devops.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.devops.service.dto.GatewayParamDTO;

public interface GatewayParamService {
    GatewayParamDTO save(GatewayParamDTO gatewayParamDTO);

    List<GatewayParamDTO> save(MultipartFile multipartFile) throws IOException;

    Boolean checkFile(MultipartFile multipartFile);

    GatewayParamDTO update(GatewayParamDTO gatewayParamDTO);

    Optional<GatewayParamDTO> partialUpdate(GatewayParamDTO gatewayParamDTO);

    Page<GatewayParamDTO> findAll(Pageable pageable);

    Optional<GatewayParamDTO> findOne(Long id);
}
