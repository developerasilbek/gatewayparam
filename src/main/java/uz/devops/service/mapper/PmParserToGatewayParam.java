package uz.devops.service.mapper;

import org.mapstruct.Mapper;
import uz.devops.domain.GatewayParam;
import uz.devops.pm.parser.payload.PmParser;

@Mapper(componentModel = "spring")
public interface PmParserToGatewayParam extends EntityMapper<PmParser, GatewayParam> {}
