package uz.devops.service.mapper;

import org.mapstruct.*;
import uz.devops.domain.GatewayParam;
import uz.devops.service.dto.GatewayParamDTO;

/**
 * Mapper for the entity {@link GatewayParam} and its DTO {@link GatewayParamDTO}.
 */
@Mapper(componentModel = "spring")
public interface GatewayParamMapper extends EntityMapper<GatewayParamDTO, GatewayParam> {}
