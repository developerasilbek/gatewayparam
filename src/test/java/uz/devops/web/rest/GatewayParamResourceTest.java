package uz.devops.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.IntegrationTest;
import uz.devops.domain.GatewayParam;
import uz.devops.domain.enumeration.MethodType;
import uz.devops.domain.enumeration.ModeType;
import uz.devops.domain.enumeration.ProtocolType;
import uz.devops.domain.enumeration.RequestType;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.service.impl.PmParserServiceImpl;
import uz.devops.repository.GatewayParamRepository;
import uz.devops.service.dto.GatewayParamDTO;
import uz.devops.service.mapper.GatewayParamMapper;

@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GatewayParamResourceTest {

    private static final String DEFAULT_URL = "http://localhost:9000/kun.uz";
    private static final String UPDATED_URL = "http://localhost:9000/api/qalampir.uz";

    private static final RequestType TYPE = RequestType.CHECK;
    private static final uz.devops.pm.parser.payload.enumeration.RequestType PARSER_TYPE =
        uz.devops.pm.parser.payload.enumeration.RequestType.CHECK;
    private static final RequestType UPDATED_TYPE = RequestType.PAY;

    private static final String USERNAME = "admin";
    private static final String UPDATED_USERNAME = "user";

    private static final String PASSWORD = "admin";
    private static final String UPDATED_PASSWORD = "user";

    private static final String TOKEN =
        "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTY3MjEyMDM1M30.edsjiQUyFfSRE3UZunt8jB52smU04uQr5A2dagdjbMN-nApEIeTFHjNDhBwLAmMOLWEyHzTCsT7yo5vHklDt7g";
    private static final String UPDATED_TOKEN =
        "oooobGciOiooooUxMiJ9.oooodWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTY3MjEyMDM1M30.edsjiQUyFfSRE3UZunt8jB52smU04uQr5A2dagdjbMN-nApEIeTFHjNDhBwLAmMOLWEyHzTCsT7yo5vHklDt7g";

    private static final String BODY =
        "Body{mode='raw', formdata=null, raw='{\n" +
        "    \"f2Key1\": \"f2Value1\",\n" +
        "    \"f2Key2\": \"f2Value2\"\n" +
        "}', options=Options{raw=Raw{language='json'}}}";

    private static final String UPDATED_BODY = "updated_body";

    private static final String STATUS_QUERY = "status_query";
    private static final String UPDATED_STATUS_QUERY = "updated_status_query";

    private static final String MESSAGE_QUERY = "message_query";
    private static final String UPDATED_MESSAGE_QUERY = "updated_message_query";

    private static final String SUCCESS_CODE = "200";
    private static final String UPDATED_SUCCESS_CODE = "201";

    private static final String CALLBACK = "callback";
    private static final String UPDATED_CALLBACK = "updated_callback";

    private static final Integer TIMEOUT = 5;
    private static final Integer UPDATED_TIMEOUT = 10;

    private static final String HEADERS = "headers";
    private static final String UPDATED_HEADERS = "updated_headers";

    private static final String DATA_QUERIES = "data_queries";
    private static final String UPDATED_DATA_QUERIES = "updated_data_queries";

    private static final MethodType HTTP_METHOD_TYPE = MethodType.GET;
    private static final uz.devops.pm.parser.payload.enumeration.MethodType PARSER_HTTP_METHOD_TYPE =
        uz.devops.pm.parser.payload.enumeration.MethodType.GET;
    private static final MethodType UPDATED_HTTP_METHOD_TYPE = MethodType.POST;

    private static final ProtocolType PROTOCOL = ProtocolType.REST;
    private static final uz.devops.pm.parser.payload.enumeration.ProtocolType PARSER_PROTOCOL =
        uz.devops.pm.parser.payload.enumeration.ProtocolType.REST;
    private static final ProtocolType UPDATED_PROTOCOL = ProtocolType.WS;

    private static final String REQUEST_MEDIA_TYPE = "request_media_type";
    private static final String UPDATED_REQUEST_MEDIA_TYPE = "updated_request_media_type";

    private static final String RESPONSE_MEDIA_TYPE = "response_media_type";
    private static final String UPDATED_RESPONSE_MEDIA_TYPE = "updated_response_media_type";

    private static final Boolean ACTIVE = true;
    private static final Boolean UPDATED_ACTIVE = false;

    private static final ModeType MODE = ModeType.TEST;
    private static final uz.devops.pm.parser.payload.enumeration.ModeType PARSER_MODE =
        uz.devops.pm.parser.payload.enumeration.ModeType.TEST;
    private static final ModeType UPDATED_MODE = ModeType.PROD;

    private static final String EXT_ID_QUERY = "ext_id_query";
    private static final String UPDATED_EXT_ID_QUERY = "updated_ext_id_query";

    private static final Boolean SUCCESS_ON_2_XX = true;
    private static final Boolean UPDATED_SUCCESS_ON_2_XX = false;

    private static final String ENTITY_API_URL = "/api/gatewayparams";
    private static final String ENTITY_API_POSTMAN_URL = "/api/gatewayparams/postman";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private GatewayParamRepository gatewayParamRepository;

    @Autowired
    private GatewayParamMapper gatewayParamMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGatewayParamMockMvc;

    @MockBean
    PmParserServiceImpl pmParserService;

    private GatewayParam gatewayParam;

    public static GatewayParam createEntity(EntityManager em) {
        GatewayParam gatewayParam = new GatewayParam();
        gatewayParam.setUrl(DEFAULT_URL);
        gatewayParam.setType(TYPE);
        gatewayParam.setUsername(USERNAME);
        gatewayParam.setPassword(PASSWORD);
        gatewayParam.setToken(TOKEN);
        gatewayParam.setBody(BODY);
        gatewayParam.setStatusQuery(STATUS_QUERY);
        gatewayParam.setMessageQuery(MESSAGE_QUERY);
        gatewayParam.setSuccessCode(SUCCESS_CODE);
        gatewayParam.setCallback(CALLBACK);
        gatewayParam.setTimeout(TIMEOUT);
        gatewayParam.setHeaders(HEADERS);
        gatewayParam.setDataQueries(DATA_QUERIES);
        gatewayParam.setHttpMethodType(HTTP_METHOD_TYPE);
        gatewayParam.setProtocol(PROTOCOL);
        gatewayParam.setRequestMediaType(REQUEST_MEDIA_TYPE);
        gatewayParam.setResponseMediaType(RESPONSE_MEDIA_TYPE);
        gatewayParam.setActive(ACTIVE);
        gatewayParam.setMode(MODE);
        gatewayParam.setExtIdQuery(EXT_ID_QUERY);
        gatewayParam.setSuccessOn2xx(SUCCESS_ON_2_XX);
        return gatewayParam;
    }

    @BeforeEach
    public void initTest() {
        gatewayParam = createEntity(em);
    }

    @Test
    @Transactional
    void createGatewayParamPostman() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
            "file",
            "test.postman_collection.json",
            MediaType.TEXT_PLAIN_VALUE,
            "Test Postman Collection!".getBytes()
        );

        List<PmParser> pmParserList = new ArrayList<>();
        PmParser pmParser = new PmParser(
            DEFAULT_URL,
            ACTIVE,
            PARSER_TYPE,
            USERNAME,
            PASSWORD,
            TOKEN,
            BODY,
            STATUS_QUERY,
            MESSAGE_QUERY,
            SUCCESS_CODE,
            CALLBACK,
            TIMEOUT,
            HEADERS,
            DATA_QUERIES,
            PARSER_HTTP_METHOD_TYPE,
            PARSER_PROTOCOL,
            REQUEST_MEDIA_TYPE,
            RESPONSE_MEDIA_TYPE,
            PARSER_MODE,
            EXT_ID_QUERY,
            SUCCESS_ON_2_XX
        );
        pmParserList.add(pmParser);
        Mockito.doReturn(pmParserList).when(pmParserService).parse(ArgumentMatchers.any());

        int databaseSizeBeforeCreate = gatewayParamRepository.findAll().size();
        restGatewayParamMockMvc
            .perform(multipart(ENTITY_API_POSTMAN_URL).file(file))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(TYPE.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(PASSWORD)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(TOKEN)))
            .andExpect(jsonPath("$.[*].body").value(hasItem(BODY)))
            .andExpect(jsonPath("$.[*].statusQuery").value(hasItem(STATUS_QUERY)))
            .andExpect(jsonPath("$.[*].messageQuery").value(hasItem(MESSAGE_QUERY)))
            .andExpect(jsonPath("$.[*].successCode").value(hasItem(SUCCESS_CODE)))
            .andExpect(jsonPath("$.[*].callback").value(hasItem(CALLBACK)))
            .andExpect(jsonPath("$.[*].timeout").value(hasItem(TIMEOUT)))
            .andExpect(jsonPath("$.[*].headers").value(hasItem(HEADERS)))
            .andExpect(jsonPath("$.[*].dataQueries").value(hasItem(DATA_QUERIES)))
            .andExpect(jsonPath("$.[*].httpMethodType").value(hasItem(HTTP_METHOD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].protocol").value(hasItem(PROTOCOL.toString())))
            .andExpect(jsonPath("$.[*].requestMediaType").value(hasItem(REQUEST_MEDIA_TYPE)))
            .andExpect(jsonPath("$.[*].responseMediaType").value(hasItem(RESPONSE_MEDIA_TYPE)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].mode").value(hasItem(MODE.toString())))
            .andExpect(jsonPath("$.[*].extIdQuery").value(hasItem(EXT_ID_QUERY)))
            .andExpect(jsonPath("$.[*].successOn2xx").value(hasItem(SUCCESS_ON_2_XX.booleanValue())));
        //        List<GatewayParam> gatewayParamList = gatewayParamRepository.findAll();
        //        assertThat(gatewayParamList).hasSize(databaseSizeBeforeCreate + 1);
        //        GatewayParam testGatewayParam = gatewayParamList.get(gatewayParamList.size() - 1);
        //        assertThat(testGatewayParam.getUrl()).isEqualTo(DEFAULT_URL);
        //        assertThat(testGatewayParam.getType()).isEqualTo(TYPE);
        //        assertThat(testGatewayParam.getUsername()).isEqualTo(USERNAME);
        //        assertThat(testGatewayParam.getPassword()).isEqualTo(PASSWORD);
        //        assertThat(testGatewayParam.getToken()).isEqualTo(TOKEN);
        //        assertThat(testGatewayParam.getBody()).isEqualTo(BODY);
        //        assertThat(testGatewayParam.getStatusQuery()).isEqualTo(STATUS_QUERY);
        //        assertThat(testGatewayParam.getMessageQuery()).isEqualTo(MESSAGE_QUERY);
        //        assertThat(testGatewayParam.getSuccessCode()).isEqualTo(SUCCESS_CODE);
        //        assertThat(testGatewayParam.getCallback()).isEqualTo(CALLBACK);
        //        assertThat(testGatewayParam.getTimeout()).isEqualTo(TIMEOUT);
        //        assertThat(testGatewayParam.getHeaders()).isEqualTo(HEADERS);
        //        assertThat(testGatewayParam.getDataQueries()).isEqualTo(DATA_QUERIES);
        //        assertThat(testGatewayParam.getHttpMethodType()).isEqualTo(HTTP_METHOD_TYPE);
        //        assertThat(testGatewayParam.getProtocol()).isEqualTo(PROTOCOL);
        //        assertThat(testGatewayParam.getRequestMediaType()).isEqualTo(REQUEST_MEDIA_TYPE);
        //        assertThat(testGatewayParam.getResponseMediaType()).isEqualTo(RESPONSE_MEDIA_TYPE);
        //        assertThat(testGatewayParam.getActive()).isEqualTo(ACTIVE);
        //        assertThat(testGatewayParam.getMode()).isEqualTo(MODE);
        //        assertThat(testGatewayParam.getExtIdQuery()).isEqualTo(EXT_ID_QUERY);
        //        assertThat(testGatewayParam.getSuccessOn2xx()).isEqualTo(SUCCESS_ON_2_XX);

        //        System.out.println("Created GatewayParam : {}\n" + gatewayParamList + "\n");
    }

    @Test
    @Transactional
    void createGatewayParam() throws Exception {
        int databaseSizeBeforeCreate = gatewayParamRepository.findAll().size();
        GatewayParamDTO gatewayParamDTO = gatewayParamMapper.toDto(gatewayParam);
        restGatewayParamMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gatewayParamDTO))
            )
            .andExpect(status().isCreated());

        List<GatewayParam> gatewayParamList = gatewayParamRepository.findAll();
        assertThat(gatewayParamList).hasSize(databaseSizeBeforeCreate + 1);
        GatewayParam testGatewayParam = gatewayParamList.get(gatewayParamList.size() - 1);
        assertThat(testGatewayParam.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testGatewayParam.getType()).isEqualTo(TYPE);
        assertThat(testGatewayParam.getUsername()).isEqualTo(USERNAME);
        assertThat(testGatewayParam.getPassword()).isEqualTo(PASSWORD);
        assertThat(testGatewayParam.getToken()).isEqualTo(TOKEN);
        assertThat(testGatewayParam.getBody()).isEqualTo(BODY);
        assertThat(testGatewayParam.getStatusQuery()).isEqualTo(STATUS_QUERY);
        assertThat(testGatewayParam.getMessageQuery()).isEqualTo(MESSAGE_QUERY);
        assertThat(testGatewayParam.getSuccessCode()).isEqualTo(SUCCESS_CODE);
        assertThat(testGatewayParam.getCallback()).isEqualTo(CALLBACK);
        assertThat(testGatewayParam.getTimeout()).isEqualTo(TIMEOUT);
        assertThat(testGatewayParam.getHeaders()).isEqualTo(HEADERS);
        assertThat(testGatewayParam.getDataQueries()).isEqualTo(DATA_QUERIES);
        assertThat(testGatewayParam.getHttpMethodType()).isEqualTo(HTTP_METHOD_TYPE);
        assertThat(testGatewayParam.getProtocol()).isEqualTo(PROTOCOL);
        assertThat(testGatewayParam.getRequestMediaType()).isEqualTo(REQUEST_MEDIA_TYPE);
        assertThat(testGatewayParam.getResponseMediaType()).isEqualTo(RESPONSE_MEDIA_TYPE);
        assertThat(testGatewayParam.getActive()).isEqualTo(ACTIVE);
        assertThat(testGatewayParam.getMode()).isEqualTo(MODE);
        assertThat(testGatewayParam.getExtIdQuery()).isEqualTo(EXT_ID_QUERY);
        assertThat(testGatewayParam.getSuccessOn2xx()).isEqualTo(SUCCESS_ON_2_XX);

        System.out.println("Created GatewayParam : {}\n" + gatewayParamList + "\n");
    }

    @Test
    @Transactional
    void getAllGatewayParams() throws Exception {
        gatewayParamRepository.saveAndFlush(gatewayParam);

        restGatewayParamMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gatewayParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(TYPE.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(PASSWORD)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(TOKEN)))
            .andExpect(jsonPath("$.[*].body").value(hasItem(BODY)))
            .andExpect(jsonPath("$.[*].statusQuery").value(hasItem(STATUS_QUERY)))
            .andExpect(jsonPath("$.[*].messageQuery").value(hasItem(MESSAGE_QUERY)))
            .andExpect(jsonPath("$.[*].successCode").value(hasItem(SUCCESS_CODE)))
            .andExpect(jsonPath("$.[*].callback").value(hasItem(CALLBACK)))
            .andExpect(jsonPath("$.[*].timeout").value(hasItem(TIMEOUT)))
            .andExpect(jsonPath("$.[*].headers").value(hasItem(HEADERS)))
            .andExpect(jsonPath("$.[*].dataQueries").value(hasItem(DATA_QUERIES)))
            .andExpect(jsonPath("$.[*].httpMethodType").value(hasItem(HTTP_METHOD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].protocol").value(hasItem(PROTOCOL.toString())))
            .andExpect(jsonPath("$.[*].requestMediaType").value(hasItem(REQUEST_MEDIA_TYPE)))
            .andExpect(jsonPath("$.[*].responseMediaType").value(hasItem(RESPONSE_MEDIA_TYPE)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].mode").value(hasItem(MODE.toString())))
            .andExpect(jsonPath("$.[*].extIdQuery").value(hasItem(EXT_ID_QUERY)))
            .andExpect(jsonPath("$.[*].successOn2xx").value(hasItem(SUCCESS_ON_2_XX.booleanValue())));
    }

    @Test
    @Transactional
    void getGatewayParam() throws Exception {
        gatewayParamRepository.saveAndFlush(gatewayParam);

        restGatewayParamMockMvc
            .perform(get(ENTITY_API_URL_ID, gatewayParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(gatewayParam.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL))
            .andExpect(jsonPath("$.type").value(TYPE.toString()))
            .andExpect(jsonPath("$.username").value(USERNAME))
            .andExpect(jsonPath("$.password").value(PASSWORD))
            .andExpect(jsonPath("$.token").value(TOKEN))
            .andExpect(jsonPath("$.body").value(BODY))
            .andExpect(jsonPath("$.statusQuery").value(STATUS_QUERY))
            .andExpect(jsonPath("$.messageQuery").value(MESSAGE_QUERY))
            .andExpect(jsonPath("$.successCode").value(SUCCESS_CODE))
            .andExpect(jsonPath("$.callback").value(CALLBACK))
            .andExpect(jsonPath("$.timeout").value(TIMEOUT))
            .andExpect(jsonPath("$.headers").value(HEADERS))
            .andExpect(jsonPath("$.dataQueries").value(DATA_QUERIES))
            .andExpect(jsonPath("$.httpMethodType").value(HTTP_METHOD_TYPE.toString()))
            .andExpect(jsonPath("$.protocol").value(PROTOCOL.toString()))
            .andExpect(jsonPath("$.requestMediaType").value(REQUEST_MEDIA_TYPE))
            .andExpect(jsonPath("$.responseMediaType").value(RESPONSE_MEDIA_TYPE))
            .andExpect(jsonPath("$.active").value(ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.mode").value(MODE.toString()))
            .andExpect(jsonPath("$.extIdQuery").value(EXT_ID_QUERY))
            .andExpect(jsonPath("$.successOn2xx").value(SUCCESS_ON_2_XX.booleanValue()));
    }

    @Test
    @Transactional
    void putExistingGatewayParam() throws Exception {
        gatewayParamRepository.saveAndFlush(gatewayParam);

        int databaseSizeBeforeUpdate = gatewayParamRepository.findAll().size();

        GatewayParam updatedGatewayParam = gatewayParamRepository.findById(gatewayParam.getId()).get();

        em.detach(updatedGatewayParam);
        updatedGatewayParam
            .url(UPDATED_URL)
            .type(UPDATED_TYPE)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .token(UPDATED_TOKEN)
            .body(UPDATED_BODY)
            .statusQuery(UPDATED_STATUS_QUERY)
            .messageQuery(UPDATED_MESSAGE_QUERY)
            .successCode(UPDATED_SUCCESS_CODE)
            .callback(UPDATED_CALLBACK)
            .timeout(UPDATED_TIMEOUT)
            .headers(UPDATED_HEADERS)
            .dataQueries(UPDATED_DATA_QUERIES)
            .httpMethodType(UPDATED_HTTP_METHOD_TYPE)
            .protocol(UPDATED_PROTOCOL)
            .requestMediaType(UPDATED_REQUEST_MEDIA_TYPE)
            .responseMediaType(UPDATED_RESPONSE_MEDIA_TYPE)
            .active(UPDATED_ACTIVE)
            .mode(UPDATED_MODE)
            .extIdQuery(UPDATED_EXT_ID_QUERY)
            .successOn2xx(UPDATED_SUCCESS_ON_2_XX);
        GatewayParamDTO gatewayParamDTO = gatewayParamMapper.toDto(updatedGatewayParam);

        restGatewayParamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, gatewayParamDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(gatewayParamDTO))
            )
            .andExpect(status().isOk());

        List<GatewayParam> gatewayParamList = gatewayParamRepository.findAll();
        assertThat(gatewayParamList).hasSize(databaseSizeBeforeUpdate);
        GatewayParam testGatewayParam = gatewayParamList.get(gatewayParamList.size() - 1);
        assertThat(testGatewayParam.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testGatewayParam.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testGatewayParam.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testGatewayParam.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testGatewayParam.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testGatewayParam.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testGatewayParam.getStatusQuery()).isEqualTo(UPDATED_STATUS_QUERY);
        assertThat(testGatewayParam.getMessageQuery()).isEqualTo(UPDATED_MESSAGE_QUERY);
        assertThat(testGatewayParam.getSuccessCode()).isEqualTo(UPDATED_SUCCESS_CODE);
        assertThat(testGatewayParam.getCallback()).isEqualTo(UPDATED_CALLBACK);
        assertThat(testGatewayParam.getTimeout()).isEqualTo(UPDATED_TIMEOUT);
        assertThat(testGatewayParam.getHeaders()).isEqualTo(UPDATED_HEADERS);
        assertThat(testGatewayParam.getDataQueries()).isEqualTo(UPDATED_DATA_QUERIES);
        assertThat(testGatewayParam.getHttpMethodType()).isEqualTo(UPDATED_HTTP_METHOD_TYPE);
        assertThat(testGatewayParam.getProtocol()).isEqualTo(UPDATED_PROTOCOL);
        assertThat(testGatewayParam.getRequestMediaType()).isEqualTo(UPDATED_REQUEST_MEDIA_TYPE);
        assertThat(testGatewayParam.getResponseMediaType()).isEqualTo(UPDATED_RESPONSE_MEDIA_TYPE);
        assertThat(testGatewayParam.getActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testGatewayParam.getMode()).isEqualTo(UPDATED_MODE);
        assertThat(testGatewayParam.getExtIdQuery()).isEqualTo(UPDATED_EXT_ID_QUERY);
        assertThat(testGatewayParam.getSuccessOn2xx()).isEqualTo(UPDATED_SUCCESS_ON_2_XX);
    }

    @Test
    @Transactional
    void deleteGatewayParam() throws Exception {
        gatewayParamRepository.saveAndFlush(gatewayParam);

        int databaseSizeBeforeDelete = gatewayParamRepository.findAll().size();

        restGatewayParamMockMvc
            .perform(delete(ENTITY_API_URL_ID, gatewayParam.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<GatewayParam> gatewayParamList = gatewayParamRepository.findAll();
        assertThat(gatewayParamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
